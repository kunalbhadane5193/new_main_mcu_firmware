################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/automode.c \
../Src/compass.c \
../Src/ds1307.c \
../Src/gpio.c \
../Src/main.c \
../Src/motor.c \
../Src/spi.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_it.c \
../Src/suntracking.c \
../Src/system_stm32f4xx.c \
../Src/timer.c \
../Src/usart.c \
../Src/windsensor.c 

OBJS += \
./Src/automode.o \
./Src/compass.o \
./Src/ds1307.o \
./Src/gpio.o \
./Src/main.o \
./Src/motor.o \
./Src/spi.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_it.o \
./Src/suntracking.o \
./Src/system_stm32f4xx.o \
./Src/timer.o \
./Src/usart.o \
./Src/windsensor.o 

C_DEPS += \
./Src/automode.d \
./Src/compass.d \
./Src/ds1307.d \
./Src/gpio.d \
./Src/main.d \
./Src/motor.d \
./Src/spi.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_it.d \
./Src/suntracking.d \
./Src/system_stm32f4xx.d \
./Src/timer.d \
./Src/usart.d \
./Src/windsensor.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F411xE -I"C:/Users/User/Desktop/main_board_mcu_firmware/Inc" -I"C:/Users/User/Desktop/main_board_mcu_firmware/Drivers/STM32F4xx_HAL_Driver/Inc" -I"C:/Users/User/Desktop/main_board_mcu_firmware/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"C:/Users/User/Desktop/main_board_mcu_firmware/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"C:/Users/User/Desktop/main_board_mcu_firmware/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


