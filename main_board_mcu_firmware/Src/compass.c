/*
 * compass.c
 *
 *  Created on: May 24, 2018
 *      Author: Kunal Bhadane
 */

#include <math.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_hal.h"

#include "gpio.h"
#include "compass.h"
#include "ds1307.h"
#include "usart.h"

extern I2C_HandleTypeDef hi2c1;

static uint8_t rx_buffer[6] = {0};

/* LSM303DLHC ACC struct */
typedef struct
{
  uint8_t Power_Mode;                         /* Power-down/Normal Mode */
  uint8_t AccOutput_DataRate;                 /* OUT data rate */
  uint8_t Axes_Enable;                        /* Axes enable */
  uint8_t High_Resolution;                    /* High Resolution enabling/disabling */
  uint8_t BlockData_Update;                   /* Block Data Update */
  uint8_t Endianness;                         /* Endian Data selection */
  uint8_t AccFull_Scale;                      /* Full Scale selection */
}LSM303DLHCAcc_InitTypeDef;

/* LSM303DLHC Mag struct */
typedef struct
{
  uint8_t Temperature_Sensor;                /* Temperature sensor enable/disable */
  uint8_t MagOutput_DataRate;                /* OUT data rate */
  uint8_t Working_Mode;                      /* operating mode */
  uint8_t MagFull_Scale;                     /* Full Scale selection */
}LSM303DLHCMag_InitTypeDef;

typedef struct lsm303AccelData_s
{
    float x;
    float y;
    float z;
} lsm303AccelData;

typedef struct lsm303MagData_s
{
	float x;
	float y;
	float z;
} lsm303MagData;

static int16_t acc_raw_x = 0, acc_raw_y = 0, acc_raw_z =0;
static int16_t mag_raw_x = 0, mag_raw_y = 0, mag_raw_z = 0;

static void LSM303DLHC_AccInit(LSM303DLHCAcc_InitTypeDef *LSM303DLHC_InitStruct);
static void LSM303DLHC_MagInit(LSM303DLHCMag_InitTypeDef *LSM303DLHC_InitStruct);
static void LSM303DLHC_Write(uint8_t DeviceAddr, uint8_t RegAddr, uint8_t* pBuffer);
static uint8_t *LSM303DLHC_Read(uint8_t DeviceAddr, uint8_t RegAddr, uint16_t NumByteToRead);
static void Get_Accelerometer_Data(struct lsm303AccelData_s *accelerometer);
static void Get_Magnetometer_Data(struct lsm303MagData_s *magnetometer);

void Compass_Init(void)
{
	LSM303DLHCAcc_InitTypeDef LSM303DLHCAcc_InitStructure;
	LSM303DLHCMag_InitTypeDef LSM303DLHCMag_InitStructure;

	LSM303DLHCAcc_InitStructure.Power_Mode = LSM303DLHC_NORMAL_MODE;
	LSM303DLHCAcc_InitStructure.AccOutput_DataRate = LSM303DLHC_ODR_100_HZ;
	LSM303DLHCAcc_InitStructure.Axes_Enable= LSM303DLHC_AXES_ENABLE;
	LSM303DLHCAcc_InitStructure.AccFull_Scale = LSM303DLHC_FULLSCALE_2G;
	LSM303DLHCAcc_InitStructure.BlockData_Update = LSM303DLHC_BlockUpdate_Continous;
	LSM303DLHCAcc_InitStructure.Endianness=LSM303DLHC_BLE_LSB;
	LSM303DLHCAcc_InitStructure.High_Resolution=LSM303DLHC_HR_ENABLE;

	LSM303DLHC_AccInit(&LSM303DLHCAcc_InitStructure);

	LSM303DLHCMag_InitStructure.Temperature_Sensor = LSM303DLHC_TEMPSENSOR_DISABLE;
	LSM303DLHCMag_InitStructure.MagOutput_DataRate = LSM303DLHC_ODR_75_HZ;
	LSM303DLHCMag_InitStructure.Working_Mode = LSM303DLHC_CONTINUOS_CONVERSION;
	LSM303DLHCMag_InitStructure.MagFull_Scale = LSM303DLHC_FS_1_3_GA;

	LSM303DLHC_MagInit(&LSM303DLHCMag_InitStructure);

}

/**
  * @brief  Set LSM303DLHC Initialization.
  * @param  LSM303DLHC_InitStruct: pointer to a LSM303DLHC_InitTypeDef structure
  *         that contains the configuration setting for the LSM303DLHC.
  * @retval None
  */
void LSM303DLHC_AccInit(LSM303DLHCAcc_InitTypeDef *LSM303DLHC_InitStruct)
{
  uint8_t ctrl1 = 0x00, ctrl4 = 0x00;

  /* Configure MEMS: data rate, power mode, full scale and axes */
  ctrl1 |= (uint8_t) (LSM303DLHC_InitStruct->Power_Mode | LSM303DLHC_InitStruct->AccOutput_DataRate | \
                    LSM303DLHC_InitStruct->Axes_Enable);

  ctrl4 |= (uint8_t) (LSM303DLHC_InitStruct->BlockData_Update | LSM303DLHC_InitStruct->Endianness | \
                    LSM303DLHC_InitStruct->AccFull_Scale|LSM303DLHC_InitStruct->High_Resolution);

  /* Write value to ACC MEMS CTRL_REG1 regsister */
  LSM303DLHC_Write(LSM303_ADDRESS_ACCEL, LSM303DLHC_CTRL_REG1_A, &ctrl1);

  /* Write value to ACC MEMS CTRL_REG4 regsister */
  LSM303DLHC_Write(LSM303_ADDRESS_ACCEL, LSM303DLHC_CTRL_REG4_A, &ctrl4);
}

/**
  * @brief  Set LSM303DLHC Mag Initialization.
  * @param  LSM303DLHC_InitStruct: pointer to a LSM303DLHC_MagInitTypeDef structure
  *         that contains the configuration setting for the LSM303DLHC.
  * @retval None
  */
void LSM303DLHC_MagInit(LSM303DLHCMag_InitTypeDef *LSM303DLHC_InitStruct)
{
  uint8_t cra_regm = 0x00, crb_regm = 0x00, mr_regm = 0x00;

  /* Configure MEMS: temp and Data rate */
  cra_regm |= (uint8_t) (LSM303DLHC_InitStruct->Temperature_Sensor | LSM303DLHC_InitStruct->MagOutput_DataRate);

  /* Configure MEMS: full Scale */
  crb_regm |= (uint8_t) (LSM303DLHC_InitStruct->MagFull_Scale);

  /* Configure MEMS: working mode */
  mr_regm |= (uint8_t) (LSM303DLHC_InitStruct->Working_Mode);

  /* Write value to Mag MEMS CRA_REG regsister */
  LSM303DLHC_Write(LSM303_ADDRESS_MAG, LSM303DLHC_CRA_REG_M, &cra_regm);

  /* Write value to Mag MEMS CRB_REG regsister */
  LSM303DLHC_Write(LSM303_ADDRESS_MAG, LSM303DLHC_CRB_REG_M, &crb_regm);

  /* Write value to Mag MEMS MR_REG regsister */
  LSM303DLHC_Write(LSM303_ADDRESS_MAG, LSM303DLHC_MR_REG_M, &mr_regm);
}

void LSM303DLHC_Write(uint8_t DeviceAddr, uint8_t RegAddr, uint8_t* pBuffer)
{
	static uint8_t tx_buffer[2];

	tx_buffer[0] = RegAddr;
	tx_buffer[1] = *pBuffer;

	if(HAL_I2C_Master_Transmit(&hi2c1, DeviceAddr, tx_buffer, 2, 1000) != HAL_OK)
	{
		//UART_Println("Error");
		my_printf("Error = %ld\n",hi2c1.ErrorCode);
	}
}

uint8_t *LSM303DLHC_Read(uint8_t DeviceAddr, uint8_t RegAddr, uint16_t NumByteToRead)
{
	memset(rx_buffer, 0, sizeof(rx_buffer));

	if(hi2c1.State == HAL_I2C_STATE_READY) {
		if(HAL_I2C_Master_Transmit(&hi2c1, DeviceAddr, &RegAddr, 1, 1000) != HAL_OK)
		{
			//UART_Println("Error");
			my_printf("Error = %ld\n",hi2c1.ErrorCode);
		}
	}

	if(hi2c1.State == HAL_I2C_STATE_READY) {
		if(HAL_I2C_Master_Receive(&hi2c1, DeviceAddr, rx_buffer, NumByteToRead, 1000) != HAL_OK)
		{
			my_printf("Error = %ld\n",hi2c1.ErrorCode);
		}
	}

	return rx_buffer;
}

void Get_Accelerometer_Data(struct lsm303AccelData_s *accelerometer)
{
	uint8_t *acc_data = LSM303DLHC_Read(LSM303_ADDRESS_ACCEL, LSM303DLHC_OUT_X_L_A | 0x80, 6);

	acc_raw_x = (int16_t)(acc_data[0] | (acc_data[1] << 8)); //>> 4;
	acc_raw_y = (int16_t)(acc_data[2] | (acc_data[3] << 8)); //>> 4;
	acc_raw_z = (int16_t)(acc_data[4] | (acc_data[5] << 8)); //>> 4;

	/* filter to get stable the value*/
	accelerometer->x = (float)acc_raw_x / 16.0; //* 0.02f + accelerometer->x * 0.98f;
	accelerometer->y = (float)acc_raw_y / 16.0; //* 0.02f + accelerometer->y * 0.98f;
	accelerometer->z = (float)acc_raw_z / 16.0;//* 0.02f + accelerometer->z * 0.98f;

	//my_printf("%0.2f\t  %0.2f\t %0.2f\n",accelerometer->x,accelerometer->y,accelerometer->z);
}

void Get_Magnetometer_Data(struct lsm303MagData_s *magnetometer)
{
	uint8_t *mag_data = LSM303DLHC_Read(LSM303_ADDRESS_MAG, LSM303DLHC_OUT_X_H_M, 6);

	mag_raw_x = (int16_t)(mag_data[1] | (mag_data[0] << 8)) - 48; //Calibration offset for magnetometer
	mag_raw_y = (int16_t)(mag_data[5] | (mag_data[4] << 8)) - 10; //Calibration offset for magnetometer
	mag_raw_z = (int16_t)(mag_data[3] | (mag_data[2] << 8));

	/* filter to get stable the value*/
	magnetometer->x = (float)mag_raw_x / LSM303DLHC_M_SENSITIVITY_XY_1_3Ga * 100.0;
	magnetometer->y = (float)mag_raw_y / LSM303DLHC_M_SENSITIVITY_XY_1_3Ga * 100.0;
	magnetometer->z = (float)mag_raw_z / LSM303DLHC_M_SENSITIVITY_Z_1_3Ga * 100.0;

	//my_printf("%0.2f\t  %0.2f\t %0.2f\n",magnetometer->x,magnetometer->y,magnetometer->z);
}

void Calculate_Heading(float *n_heading)
{
	struct lsm303AccelData_s acc;
	struct lsm303MagData_s mag;
	float roll = 0;
	float pitch = 0;
	float heading = 0;
	static float const alpha = 0.05;
	static float mag_x, mag_y,mag_z;
	static float acc_x, acc_y,acc_z;

	Get_Accelerometer_Data(&acc);
	Get_Magnetometer_Data(&mag);

	acc_x = alpha * acc.x + (1 - alpha) * acc_x;
	acc_y = alpha * acc.y + (1 - alpha) * acc_y;
	acc_z = alpha * acc.z + (1 - alpha) * acc_z;

	//my_printf("%0.2f\t  %0.2f\t %0.2f\n",acc_x,acc_y,acc_z);

	mag_x = alpha * mag.x + (1 - alpha) * mag_x;
	mag_y = alpha * mag.y + (1 - alpha) * mag_y;
	mag_z = alpha * mag.z + (1 - alpha) * mag_z;

	//my_printf("%0.2f\t  %0.2f\t %0.2f\n",mag_x,mag_y,mag_z);

	/* Calculate roll and pitch from accelerometer data
	 * Roll and pitch will help us to keep heading angle
	 * constant when parasol tilts.
	 */
	roll = atan2f(acc_y , sqrtf((acc_x * acc_x) + (acc_z * acc_z)));
	//my_printf("r=%0.2f\n", (roll * 180) / M_PI);

	pitch = atan2f(acc_x , sqrtf((acc_y * acc_y) + (acc_z * acc_z)));
	//my_printf("p=%0.2f\n", (pitch * 180) / M_PI);

	/* Calculate new x and y component of magnetic north heading
	 *  for heading calculations.
	 */
	mag.x = (mag.x * cosf(pitch)) + (mag.z * sinf(pitch));

	mag.y = (mag.x * sinf(roll) * sinf(pitch)) + (mag.y * cosf(roll)) - (mag.z * sinf(roll) * cosf(pitch));

	//my_printf("%0.2f\t  %0.2f\n",mag.x,mag.y);

	heading = (atan2f(mag.y , mag.x) * 180) / M_PI;

	//Normalize it to 0-360
	if(heading < 0)
	{
		heading = 360 + heading;
	}

	*n_heading = heading;
	my_printf("%0.2f\n",*n_heading);
}
