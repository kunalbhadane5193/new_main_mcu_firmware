/*
 * motor.c
 *
 *  Created on: May 9, 2018
 *      Author: Kunal Bhadane
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "motor.h"
#include "usart.h"
#include "spi.h"

Limit_Switch_Status Get_Expansion_Status(void)
{
	Expansion_SPI_Process(exp_status);
	switch(exp_limit_state)
	{
		case limit_closed:
			return limit_closed;
		case limit_opened:
			return limit_opened;
		case limit_middle:
			return limit_middle;
		default:
			return limit_middle;
	}
}

void Expansion_Open(void)
{
	Expansion_SPI_Process(exp_open);
}

void Expansion_Close(void)
{
	Expansion_SPI_Process(exp_close);
}

void Expansion_Stop(void)
{
	Expansion_SPI_Process(exp_stop);
}


Limit_Switch_Status Get_Elevation_Status(void)
{
	Elevation_SPI_Process(tilt_status);
	switch(elev_limit_state)
	{
		case limit_closed:
			return limit_closed;
		case limit_opened:
			return limit_opened;
		case limit_middle:
			return limit_middle;
		default:
			return limit_middle;
	}
}

void Tilt_Down(void)
{
	Elevation_SPI_Process(tilt_down);
}

void Tilt_Up(void)
{
	Elevation_SPI_Process(tilt_up);
}

void Tilt_To(uint8_t angle)
{
	if(tilt_angle != angle)
	{
		tilt_angle = angle;
		if(tilt_angle > 45)
		{
			tilt_angle = 45;
			//Tilt_Down();

		}
		if(tilt_angle < 0)
		{
			tilt_angle = 0;
			//Tilt_Up()
			//return;
		}
		my_printf("RoAngle=%d\n",tilt_angle);
		Elevation_SPI_Process(tilt_to);
	}
}

void Tilt_Stop(void)
{
	Elevation_SPI_Process(tilt_stop);
}

void Get_Azimuth_Status(void)
{
	Azimuth_SPI_Process(rotation_status);
	/*switch(azi_state)
	{
		case rotating_right:
			return rotating_right;
		case rotating_left:
			return rotating_left;
		case rotation_stopped:
			return rotation_stopped;
		case calibration_complete:
			calibration_done = true;
			return calibration_complete;
		default:
			return rotation_stopped;
	}*/
	if(azi_state == calibration_complete)
	{
		calibration_done = true;
	}
}
void Rotate_Right(void)
{
	Azimuth_SPI_Process(rotate_right);
}

void Rotate_Left(void)
{
	//Azimuth_SPI_Process(rotate_left);
}

//int16_t current_angle = 0;
void Rotate_By(uint16_t angle)
{
	/*if(angle != current_angle)
	{
		rotate_angle = angle - current_angle;
		my_printf("Azimuth:%d\n",rotate_angle);
		current_angle = angle;
		Azimuth_SPI_Process(rotate_by);
	}*/

	if(rotate_angle != angle)
	{
		rotate_angle = angle;
		if(rotate_angle < 0)
		{
			return;
		}
		if(rotate_angle > 360)
		{
			return;
		}
		my_printf("AziAngle=%d\n",rotate_angle);
		Azimuth_SPI_Process(rotate_by);
	}
}

void Rotation_Stop(void)
{
	Azimuth_SPI_Process(rotation_stop);
}

void Calibrate_Azimuth_Motor(void)
{
	//UART_Print("Calibrate\n");
	Azimuth_SPI_Process(calibrate);
	//calibration_on = true;
}
