/*
 * timer.c
 *
 *  Created on: May 10, 2018
 *      Author: User
 */

/* Includes ------------------------------------------------------------------*/
#include "timer.h"
#include "stm32f4xx_hal.h"
#include "usart.h"
#include "ds1307.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim11;

static uint16_t timer10_tick = 0;
static uint16_t timer11_tick = 0;

static void Timer10_Tick_Callback(void);
static void Timer11_Tick_Callback(void);

/* TIM10 init function */
void TIM10_Init(void)
{

  /* Calculate period based on clock frequency and desired cycle count *
   * Use PCLK1 frequency as time basis                                 */
  uint32_t tim_period = ((HAL_RCC_GetPCLK1Freq()/ 1000) * CYCLE_PERIOD_MS);
  uint32_t tim_prescaler = 1;

 /* Dynamically calculate period and prescaler */
  while (tim_period >= 65536) {
	tim_period = tim_period >> 1;
	tim_prescaler = tim_prescaler << 1;
  }

  //my_printf("Period = %d\n",tim_period);
  //my_printf("Period = %d\n",tim_prescaler);

  htim10.Instance = TIM10;
  htim10.Init.Prescaler = tim_prescaler - 1;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = tim_period;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
/* TIM11 init function */
void TIM11_Init(void)
{
  /* Calculate period based on clock frequency and desired cycle count *
   * Use PCLK1 frequency as time basis                                 */
  uint32_t tim_period = ((HAL_RCC_GetPCLK1Freq()/ 1000) * CYCLE_PERIOD_MS);
  uint32_t tim_prescaler = 1;

	/* Dynamically calculate period and prescaler */
  while (tim_period >= 65536) {
	    tim_period = tim_period >> 1;
	    tim_prescaler = tim_prescaler << 1;
  }

  htim11.Instance = TIM11;
  htim11.Init.Prescaler = tim_prescaler - 1;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = tim_period;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* Timer start function */
void Timer10_Start(void) {
  if (HAL_TIM_Base_Start_IT(&htim10) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

void Timer11_Start(void)
{
	if (HAL_TIM_Base_Start_IT(&htim11) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
}

/* Timer stop function */
void Timer10_Stop(void) {
  if (HAL_TIM_Base_Stop_IT(&htim10) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

void Timer11_Stop(void) {
	if (HAL_TIM_Base_Stop_IT(&htim11) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}
}

/* TIM Trigger callback function */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
  if (htim->Instance == htim10.Instance) {
    Timer10_Tick_Callback();
  }
  if (htim->Instance == htim11.Instance) {
      Timer11_Tick_Callback();
  }
}

void Timer10_Tick_Callback(void)
{
	if(timer10_tick >= 10)
	{
		time_updated = true;
		timer10_tick = 0;
	}

	timer10_tick++;
}

void Timer11_Tick_Callback(void)
{
	if(timer11_tick >= 200)
	{
		one_sec_updated = true;
		timer11_tick = 0;
	}

	timer11_tick++;
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM10)
  {
  /* USER CODE BEGIN TIM10_MspInit 0 */

  /* USER CODE END TIM10_MspInit 0 */
    /* TIM10 clock enable */
    __HAL_RCC_TIM10_CLK_ENABLE();

    /* TIM10 interrupt Init */
	HAL_NVIC_SetPriority(TIM1_UP_TIM10_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
  /* USER CODE BEGIN TIM10_MspInit 1 */

  /* USER CODE END TIM10_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM11)
  {
    /* TIM11 clock enable */
    __HAL_RCC_TIM11_CLK_ENABLE();

    /* TIM10 interrupt Init */
    HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM11_IRQn);
  }
}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM10)
  {
  /* USER CODE BEGIN TIM10_MspDeInit 0 */

  /* USER CODE END TIM10_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM10_CLK_DISABLE();
  /* USER CODE BEGIN TIM10_MspDeInit 1 */

  /* USER CODE END TIM10_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM11)
  {
  /* USER CODE BEGIN TIM11_MspDeInit 0 */

  /* USER CODE END TIM11_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM11_CLK_DISABLE();
  /* USER CODE BEGIN TIM11_MspDeInit 1 */

  /* USER CODE END TIM11_MspDeInit 1 */
  }
}
