/*
 * automode.c
 *
 *  Created on: May 31, 2018
 *      Author: User
 */

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"

#include "automode.h"
#include "gpio.h"
#include "spi.h"
#include "motor.h"
#include "suntracking.h"
#include "ds1307.h"
#include "usart.h"

static uint8_t button_press_count = 0;

void Automode_Button_Callback(void)
{
	GPIO_PinState button = HAL_GPIO_ReadPin(EXTERNAL_BUTTON_GPIO_Port, EXTERNAL_BUTTON_Pin);
	/* Check if is button pressed.
	 * Pressed once, turn ON automode.
	 * Pressed twice, turn OFF automode.
	 */
	if(button == 0)
	{
		button_press_count++;

		if(button_press_count == 1)
		{
			automode_on = true;
			// State machine sequence is written in Main
			// When automode is ON first task is to calibrate the azimuth motor
			auto_command = calibrate_azimuth_motor;
		}
		else if(button_press_count == 2)
		{
			button_press_count = 0;
			automode_on = false;
			auto_command = stop_auto_mode;
			Stop_Auto_Mode();
		}
	}
}

void Set_Auto_Open_Time(uint8_t hour, uint8_t minutes)
{
	auto_open_time[TIME_HOUR] = hour;
	auto_open_time[TIME_MINUTE] = minutes;
	auto_open_time[TIME_SECOND] = 0;
}

void Set_Auto_Close_Time(uint8_t hour, uint8_t minutes)
{
	auto_close_time[TIME_HOUR] = hour;
	auto_close_time[TIME_MINUTE] = minutes;
	auto_close_time[TIME_SECOND] = 0;
}

void Set_Suntracking_Parameters(void)
{
	spa.year          = current_time[TIME_YEAR];//2018;
	spa.month         = current_time[TIME_MONTH];//5;
	spa.day           = current_time[TIME_DATE];//10;
	spa.hour          = current_time[TIME_HOUR];//16;
	spa.minute        = current_time[TIME_MINUTE];//00;
	spa.second        = current_time[TIME_SECOND];//00;
	spa.timezone      = -8.0;
	spa.delta_ut1     = 0;
	spa.delta_t       = 67;
	spa.longitude     = -118.15284730000002;
	spa.latitude      = 34.1403839;
	spa.elevation     = 249;
	spa.pressure      = 820;
	spa.temperature   = 11;
	spa.slope         = 30;
	spa.azm_rotation  = -10;
	spa.atmos_refract = 0.5667;
	spa.function      = SPA_ALL;
}

void Start_Tracking(void)
{
	static int result;

	result = spa_calculate(&spa);

	if (result == 0)  //check for SPA errors
	{
		//display the results inside the SPA structure

		//my_printf("Julian Day:    %.6f\n",spa.jd);
		//my_printf("L:             %.6e degrees\n",spa.l);
		//my_printf("B:             %.6e degrees\n",spa.b);
		//my_printf("R:             %.6f AU\n",spa.r);
		//my_printf("H:             %.6f degrees\n",spa.h);
		//my_printf("Delta Psi:     %.6e degrees\n",spa.del_psi);
		//my_printf("Delta Epsilon: %.6e degrees\n",spa.del_epsilon);
		//my_printf("Epsilon:       %.6f degrees\n",spa.epsilon);
		//my_printf("Zenith:        %.6f degrees\n",spa.zenith);
		my_printf("Azimuth:       %.6f degrees\n",spa.azimuth);
		//my_printf("Incidence:     %.6f degrees\n",spa.incidence);

		/*min = 60.0*(spa.sunrise - (int)(spa.sunrise));
		sec = 60.0*(min - (int)min);
		my_printf("Sunrise:       %02d:%02d:%02d Local Time\n", (int)(spa.sunrise), (int)min, (int)sec);

		min = 60.0*(spa.sunset - (int)(spa.sunset));
		sec = 60.0*(min - (int)min);
		my_printf("Sunset:        %02d:%02d:%02d Local Time\n", (int)(spa.sunset), (int)min, (int)sec);
		*/

		Rotate_By(spa.azimuth);
		Tilt_To(spa.zenith);

	 } else my_printf("SPA Error Code: %d\n", result);
}

/* Function to stop automode.
 * Currently, we have only one mode so, turn off all the motors and suntracking.
 */
void Stop_Auto_Mode(void)
{
	automode_on = false;
	calibration_done = false;
	suntracking_on = false;
	Tilt_Up();
	Expansion_Close();
	Rotation_Stop();
}
