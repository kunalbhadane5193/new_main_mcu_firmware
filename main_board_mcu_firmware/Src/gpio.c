/*
 * gpio.c
 *
 *      Author: Kunal Bhadane
 */


#include "stm32f4xx_hal.h"

#include <stdbool.h>
#include <stdint.h>
#include "stm32f4xx_hal.h"

#include "gpio.h"
#include "automode.h"
#include "spi.h"
#include "motor.h"

void GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
   __HAL_RCC_GPIOA_CLK_ENABLE();
   __HAL_RCC_GPIOB_CLK_ENABLE();
   __HAL_RCC_GPIOC_CLK_ENABLE();
   __HAL_RCC_GPIOD_CLK_ENABLE();
   __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, EXPANSION_SPI3_NSS_Pin|ELEVATION_SPI2_NSS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, AZIMUTH_SPI2_NSS_Pin , GPIO_PIN_SET);

  /*Configure GPIO pins : EXPANSION_SPI3_NSS_Pin ELEVATION_SPI2_NSS_Pin */
  GPIO_InitStruct.Pin = EXPANSION_SPI3_NSS_Pin|ELEVATION_SPI2_NSS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : AZIMUTH_SPI2_NSS_Pin */
  GPIO_InitStruct.Pin = AZIMUTH_SPI2_NSS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin */
   GPIO_InitStruct.Pin = EXPANSION_MOTOR_INT_Pin|ELEVATION_MOTOR_INT_Pin|AZIMUTH_MOTOR_INT_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
   GPIO_InitStruct.Pull = GPIO_PULLUP;
   HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

   /*Configure GPIO pin : PtPin */
   GPIO_InitStruct.Pin = EXTERNAL_BUTTON_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
   GPIO_InitStruct.Pull = GPIO_PULLUP;
   HAL_GPIO_Init(EXTERNAL_BUTTON_GPIO_Port, &GPIO_InitStruct);

   /*Configure GPIO pins : PBPin PBPin */
   GPIO_InitStruct.Pin = WIND_INT1_Pin|WIND_INT2_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   /* EXTI interrupt init*/
   HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
   HAL_NVIC_EnableIRQ(EXTI4_IRQn);

   HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
   HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

   HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
   HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin){
		case EXPANSION_MOTOR_INT_Pin:
			//UART_Println("Exp Int");
			//Expansion_SPI_Process(exp_status);
			//Expansion_SPI_Process(exp_status);
			//Get_Expansion_Status();
			break;
		case ELEVATION_MOTOR_INT_Pin:
			//UART_Println("Elev Int");
			//Elevation_SPI_Process(tilt_status);
			//Elevation_SPI_Process(tilt_status);
			//Get_Elevation_Status();
			break;
		case AZIMUTH_MOTOR_INT_Pin:
			//UART_Println("Azi Int");
			Azimuth_SPI_Process(rotation_status);
			//Azimuth_SPI_Process(rotation_status);
			Get_Azimuth_Status();
			break;
		case EXTERNAL_BUTTON_Pin:
			Automode_Button_Callback();
			break;
	}
}
