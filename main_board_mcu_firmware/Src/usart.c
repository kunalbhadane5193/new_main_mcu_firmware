/*
 * usart.c
 *
 *  Created on: May 8, 2018
 *      Author: Kunal Bhadane
 */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "stm32f4xx_hal.h"
#include "usart.h"

UART_HandleTypeDef h_uart;

uint8_t             rx_buffer[UART_RX_BUFFER_SIZE];
bool                rx_ready;

/* UART Init Function */
void UART_Init(void) {
  h_uart.Instance = USART1;
  h_uart.Init.BaudRate = 115200;
  h_uart.Init.WordLength = UART_WORDLENGTH_8B;
  h_uart.Init.StopBits = UART_STOPBITS_1;
  h_uart.Init.Parity = UART_PARITY_NONE;
  h_uart.Init.Mode = UART_MODE_TX_RX;
  h_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  h_uart.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&h_uart) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* UART Start Acquisition Function */
void UART_Start(void) {
  /* Start recieving */
  if (HAL_UART_Receive_IT(&h_uart, rx_buffer, UART_RX_BUFFER_SIZE) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }

  /* Initialize ready flag */
  rx_ready = false;
}


/* UART Print Function */
void UART_Print(char* buffer) {
  if (HAL_UART_Transmit(&h_uart, (uint8_t*)buffer, strlen(buffer), UART_TIMEOUT_MS) != HAL_OK) {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/* UART Print Line Function */
void UART_Println(char* buffer) {
  UART_Print(buffer);
  UART_Print("\n");
}

/* UART Rx Read Function */
bool UART_Read(uint8_t* buffer) {
  if (rx_ready) {
    buffer = rx_buffer;
    rx_ready = false;
    return true;
  }
  return false;
}


void vprint(const char *fmt, va_list argp)
{
    char string[200];
    if(0 < vsprintf(string,fmt,argp)) // build string
    {
        HAL_UART_Transmit(&h_uart, (uint8_t*)string, strlen(string), 0xffffff); // send message via UART
    }
}

void my_printf(const char *fmt, ...) // custom printf() function
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

/* Private function implementation -------------------------------------------*/

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle) {

  GPIO_InitTypeDef GPIO_InitStruct;
  if(uartHandle->Instance==USART1) {
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle){

  if(uartHandle->Instance==USART1) {
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  }
}

/* UART Rx Complete Callback */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  rx_ready = true;
}



