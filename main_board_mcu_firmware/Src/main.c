
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include <math.h>

#include "main.h"
#include "stm32f4xx_hal.h"

#include "gpio.h"
#include "usart.h"
#include "spi.h"
#include "motor.h"
#include "ds1307.h"
#include "automode.h"
#include "suntracking.h"
#include "timer.h"
#include "windsensor.h"
#include "compass.h"

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim11;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_RTC_Init(void);

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  static uint16_t timeup_count = 0;
  static float north_heading;

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  GPIO_Init();

  MX_RTC_Init(); //Currently not using

  TIM10_Init();
  TIM11_Init();

  UART_Init();

  UART_Start();

  //UART_Print("Start\n");

  SPI_Init();

  I2C_Init();

  //Start 10ms timer for compass and accelerometer
  Timer10_Start();

  //Start 1 second timer for RTC and Wind Sensor
  Timer11_Start();

  /* As main mcu is running at very fast speed,
   * need this delay for other peripherals to start.
   * e.g. Compass and accelerometer, RTC and Motor drivers.
   */
  HAL_Delay(500);

  Compass_Init();

  while (1)
  {

	  /* Currently, as we have only button for control, we need
	   * two functions to control parasol. If auto mode is on,
	   * calibrate azimuth motor, open parasol either with auto
	   * open & close time or if past that time parasol will open
	   * when button is pressed.
	   * When auto mode is turned off by button, close parasol,
	   * tilt up and stop the rotation.
	   */

	  /* Using 2 timers. One for compass and other for RTC and wind sensor
	   * Only one can be used.
	   * */
	  if(time_updated)
	  {
		  time_updated = false;
		  Calculate_Heading(&north_heading);
	  }

	  // Read time and wind speed every 1 s
	  // Use timer to set 1s time

	  if(one_sec_updated)
	  {
		  one_sec_updated = false;
		  //Read_Time();
		  /*if((automode_on) && (suntracking_on))
		    {
		   	  	  ++timeup_count;
		  		  my_printf("%d\n",timeup_count);
		  	}*/
		  	//Read_Wind_Count();
	  }

	  /* Auto mode state machine which will run sequentially after button is pressed.
	   * TODO: Auto open time and close time from user
	   */
	  if(automode_on)
	  {
		  switch (auto_command)
		  {
		  	  case calibrate_azimuth_motor:
		  		  /* Calibration command set in button callback as  it
		  		   * is the first step to be performed after auto mode
		  		   * is turned ON.
		  		   */
		  		  Calibrate_Azimuth_Motor();
		  		  //Set auto open time when set by user
		  		  //Set_Auto_Open_Time(8, 0);
		  		  //Set auto close time (24 hr format)
		  		  //Set_Auto_Close_Time(18, 0);
		  		  Expansion_Open();
		  		  auto_command = wait_for_calibration;
		  		  break;
		  	  case wait_for_calibration:
		  		  //Wait for calibration to complete
		  		  if(calibration_done)
		  		  {
		  			  auto_command = find_north; //start_suntracking;
		  		  }
		  		  break;
		  	  case find_north:
		  		auto_command = start_suntracking;
		  		break;
		  	  case start_suntracking:
		  		  /* Track the Sun for the first time after calibration is done.
		  		   * Wait for 6 minutes after first Suntracking call.
		  		   */
		  		  //Read_Time();
		  		  //Set_Suntracking_Parameters();
		  		  //Start_Tracking();
		  		  auto_command = wait_for_timeup;
		  		  break;
		  	  case wait_for_timeup:
		  		  // Wait for 6 Minutes
		  		  if(timeup_count == 360)
		  		  {
		  			  timeup_count = 0;
		  			  //Read_Time();
		  			UART_Print("Track\n");
		  			//  Set_Suntracking_Parameters();
		  			//  Start_Tracking();
		  		  }
		  		  auto_command = wait_for_timeup;
		  		  break;
		  	  case stop_auto_mode:
		  		  //Stop function is called after auto mode is off
		  		  break;
		  	  default:
		  		  break;
		  }
	  }
  }

}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	/**Configure the main internal regulator output voltage
	*/
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	*/
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 200;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
	_Error_Handler(__FILE__, __LINE__);
  }

	/**Initializes the CPU, AHB and APB busses clocks
	*/
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
							  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
	_Error_Handler(__FILE__, __LINE__);
  }

  // NOTE:: LSE not working, temp fix is HSE_DIV25 (320 kHz)
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_HSE_DIV25;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
	_Error_Handler(__FILE__, __LINE__);
  }

	/**Configure the Systick interrupt time
	*/
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	*/
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* RTC init function */
static void MX_RTC_Init(void)
{

  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
if(HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR0) != 0x32F2){
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initialize RTC and set the Time and Date 
    */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR0,0x32F2);
  }

}


/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
