/*
 * spi.c
 *
 *  Created on: May 8, 2018
 *      Author: Kunal Bhadane
 */

#include <string.h>
#include "stm32f4xx_hal.h"

#include "gpio.h"
#include "spi.h"
#include "usart.h"
#include "ds1307.h"

SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;

static void Enable_Chip_Select(NSS_PIN pin);
static void Disable_Chip_Select(NSS_PIN pin);

/* SPI2 init function */
void SPI_Init(void)
{

  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
	_Error_Handler(__FILE__, __LINE__);
  }

  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}



/**
* @brief This function handles SPI2 global interrupt.
*/
void SPI2_IRQHandler(void)
{
  HAL_SPI_IRQHandler(&hspi2);
  //SPI_Process();
}

/**
* @brief This function handles SPI3 global interrupt.
*/
void SPI3_IRQHandler(void)
{
  HAL_SPI_IRQHandler(&hspi3);

}

void Expansion_SPI_Process(Expansion_Motor_Command mcommand)
{
	Enable_Chip_Select(EXPANSION_NSS_PIN);
	aTxBuffer[0] = mcommand;
	if(hspi3.State == HAL_SPI_STATE_READY)	{
		//if(HAL_SPI_TransmitReceive_IT(&hspi3, aTxBuffer, aRxBuffer, sizeof(aTxBuffer)) != HAL_OK)
		if(HAL_SPI_TransmitReceive(&hspi3, aTxBuffer, aRxBuffer, sizeof(aTxBuffer),HAL_MAX_DELAY) != HAL_OK)
		{
			UART_Println("Error");
		}
		//HAL_Delay(0);
	}
	Disable_Chip_Select(EXPANSION_NSS_PIN);
	//my_printf("Received = %x\n",aRxBuffer[0]);
	exp_limit_state = aRxBuffer[0];
}

void Elevation_SPI_Process(Elevation_Motor_Command mcommand)
{
	aTxBuffer[0] = mcommand;
	if(mcommand == tilt_to)
	{
		aTxBuffer[1] = tilt_angle;
	}
	else
	{
		aTxBuffer[1] = 0x00;
	}
	if(hspi2.State == HAL_SPI_STATE_READY)
	{
		Enable_Chip_Select(ELEVATION_NSS_PIN);
		//printf("Send State = %x\n",aTxBuffer[0]);
		//if(HAL_SPI_TransmitReceive_IT(&hspi2, aTxBuffer, aRxBuffer, sizeof(aTxBuffer)) != HAL_OK)
		if(HAL_SPI_TransmitReceive(&hspi2, aTxBuffer, aRxBuffer, sizeof(aTxBuffer),5000) != HAL_OK)
		{
			UART_Println("Error");
		}
		//HAL_Delay(0);
	}
	Disable_Chip_Select(ELEVATION_NSS_PIN);
	//my_printf("Received = %x\n",aRxBuffer[0]);
	elev_limit_state = aRxBuffer[0];
}

void Azimuth_SPI_Process(Azimuth_Motor_Command mcommand)
{
	if(mcommand == rotate_by)
	{
		aTxBuffer[0] = (uint8_t) rotate_angle; //& 0x00FF;
		aTxBuffer[1] = (rotate_angle & 0xFF00) >> 8;

		//*aTxBuffer = rotate_angle;
		//my_printf("1B = %d\n",aTxBuffer[0]);
		//my_printf("2B = %d\n",aTxBuffer[1]);
	}
	else
	{
		aTxBuffer[0] = mcommand;
		aTxBuffer[1] = 0xAB;
	}
	if(hspi2.State == HAL_SPI_STATE_READY)
	{
		Enable_Chip_Select(AZIMUTH_NSS_PIN);
		//printf("Send State = %x\n",aTxBuffer[0]);
		//if(HAL_SPI_TransmitReceive_IT(&hspi2, aTxBuffer, aRxBuffer, sizeof(aTxBuffer)) != HAL_OK)
		if(HAL_SPI_TransmitReceive(&hspi2, aTxBuffer, aRxBuffer, sizeof(aTxBuffer),5000) != HAL_OK)
		{
			UART_Println("Error");
		}
		//HAL_Delay(0);
	}
	Disable_Chip_Select(AZIMUTH_NSS_PIN);
	my_printf("Received = %x\n",aRxBuffer[0]);
	azi_state = aRxBuffer[0];
}

/*void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	//my_printf("Transferred = %x\n",aTxBuffer[0]);
	my_printf("Received = %x\n",aRxBuffer[0]);
	Disable_Chip_Select(EXPANSION_NSS_PIN);
	Disable_Chip_Select(ELEVATION_NSS_PIN);
	Disable_Chip_Select(AZIMUTH_NSS_PIN);
}*/

void Enable_Chip_Select(NSS_PIN pin)
{
	switch(pin){
		case AZIMUTH_NSS_PIN:
			HAL_GPIO_WritePin(AZIMUTH_SPI2_NSS_GPIO_Port, AZIMUTH_SPI2_NSS_Pin , GPIO_PIN_RESET);
			break;
		case ELEVATION_NSS_PIN:
			HAL_GPIO_WritePin(ELEVATION_SPI2_NSS_GPIO_Port, ELEVATION_SPI2_NSS_Pin, GPIO_PIN_RESET);
			break;
		case EXPANSION_NSS_PIN:
			HAL_GPIO_WritePin(EXPANSION_SPI3_NSS_GPIO_Port, EXPANSION_SPI3_NSS_Pin, GPIO_PIN_RESET);
			break;
		default:
			break;
	}

}

void Disable_Chip_Select(NSS_PIN pin)
{
	switch(pin){
		case AZIMUTH_NSS_PIN:
			HAL_GPIO_WritePin(AZIMUTH_SPI2_NSS_GPIO_Port, AZIMUTH_SPI2_NSS_Pin , GPIO_PIN_SET);
			break;
		case ELEVATION_NSS_PIN:
			HAL_GPIO_WritePin(ELEVATION_SPI2_NSS_GPIO_Port, ELEVATION_SPI2_NSS_Pin, GPIO_PIN_SET);
			break;
		case EXPANSION_NSS_PIN:
			HAL_GPIO_WritePin(EXPANSION_SPI3_NSS_GPIO_Port, EXPANSION_SPI3_NSS_Pin, GPIO_PIN_SET);
			break;
		default:
			break;
	}
}


void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hspi->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspInit 0 */

  /* USER CODE END SPI2_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SPI2_CLK_ENABLE();

    /**SPI2 GPIO Configuration
    PC2     ------> SPI2_MISO
    PC3     ------> SPI2_MOSI
    PC7     ------> SPI2_SCK
    PB9     ------> SPI2_NSS
    */
    GPIO_InitStruct.Pin = AZIMUTH_SPI2_MISO_Pin|AZIMUTH_SPI2_MOSI_Pin|AZIMUTH_SPI2_SCK_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /*GPIO_InitStruct.Pin = AZIMUTH_SPI2_NSS_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(AZIMUTH_SPI2_NSS_GPIO_Port, &GPIO_InitStruct);
    */

    /* SPI2 interrupt Init */
    HAL_NVIC_SetPriority(SPI2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(SPI2_IRQn);
  /* USER CODE BEGIN SPI2_MspInit 1 */

  /* USER CODE END SPI2_MspInit 1 */
  }
  else if(hspi->Instance==SPI3)
  {
  /* USER CODE BEGIN SPI3_MspInit 0 */

  /* USER CODE END SPI3_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SPI3_CLK_ENABLE();

    /**SPI3 GPIO Configuration
    PC10     ------> SPI3_SCK
    PC11     ------> SPI3_MISO
    PC12     ------> SPI3_MOSI
    */
    GPIO_InitStruct.Pin = EXPANSION_SPI3_SCK_Pin|EXPANSION_SPI3_MISO_Pin|EXPANSION_SPI3_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* SPI3 interrupt Init */
    HAL_NVIC_SetPriority(SPI3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(SPI3_IRQn);
  /* USER CODE BEGIN SPI3_MspInit 1 */

  /* USER CODE END SPI3_MspInit 1 */
  }

}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{

  if(hspi->Instance==SPI2)
  {
  /* USER CODE BEGIN SPI2_MspDeInit 0 */

  /* USER CODE END SPI2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI2_CLK_DISABLE();

    /**SPI2 GPIO Configuration
    PC2     ------> SPI2_MISO
    PC3     ------> SPI2_MOSI
    PC7     ------> SPI2_SCK
    PB9     ------> SPI2_NSS
    */
    HAL_GPIO_DeInit(GPIOC, AZIMUTH_SPI2_MISO_Pin|AZIMUTH_SPI2_MOSI_Pin|AZIMUTH_SPI2_SCK_Pin);

    HAL_GPIO_DeInit(AZIMUTH_SPI2_NSS_GPIO_Port, AZIMUTH_SPI2_NSS_Pin);

    /* SPI2 interrupt DeInit */
    HAL_NVIC_DisableIRQ(SPI2_IRQn);
  /* USER CODE BEGIN SPI2_MspDeInit 1 */

  /* USER CODE END SPI2_MspDeInit 1 */
  }
  else if(hspi->Instance==SPI3)
  {
  /* USER CODE BEGIN SPI3_MspDeInit 0 */

  /* USER CODE END SPI3_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI3_CLK_DISABLE();

    /**SPI3 GPIO Configuration
    PC10     ------> SPI3_SCK
    PC11     ------> SPI3_MISO
    PC12     ------> SPI3_MOSI
    */
    HAL_GPIO_DeInit(GPIOC, EXPANSION_SPI3_SCK_Pin|EXPANSION_SPI3_MISO_Pin|EXPANSION_SPI3_MOSI_Pin);

    /* SPI3 interrupt DeInit */
    HAL_NVIC_DisableIRQ(SPI3_IRQn);
  /* USER CODE BEGIN SPI3_MspDeInit 1 */

  /* USER CODE END SPI3_MspDeInit 1 */
  }

}
