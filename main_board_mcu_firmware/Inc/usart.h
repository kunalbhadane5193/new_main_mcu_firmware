/*
 * usart.h
 *
 *  Created on: May 8, 2018
 *      Author: Kunal Bhadane
 */

#ifndef USART_H_
#define USART_H_

#include <stdint.h>
#include <stdbool.h>

/* Private define ------------------------------------------------------------*/
#define UART_RX_BUFFER_SIZE   (256)
#define UART_TIMEOUT_MS       (1000)

/* Public function prototypes ------------------------------------------------*/
void UART_Init(void);
void UART_Start(void);
void UART_Print(char* buffer);
void UART_Println(char* buffer);
bool UART_Read(uint8_t* buffer);

void vprint(const char *fmt, va_list argp);
void my_printf(const char *fmt, ...);

extern void _Error_Handler(char *, int);

#endif /* USART_H_ */
