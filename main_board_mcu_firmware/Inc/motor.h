/*
 * motor.h
 *
 *  Created on: May 9, 2018
 *      Author: Kunal Bhadane
 */

#ifndef MOTOR_H_
#define MOTOR_H_

typedef enum {
	exp_open 	= 0x01,
	exp_close 	= 0x02,
	exp_stop 	= 0x03,
	exp_status 	= 0xAA
}Expansion_Motor_Command;

typedef enum {
	exp_opening = 0xA0,
	exp_closing = 0xB0,
	exp_stopped = 0xC0
}Expansion_Motor_Status;

typedef enum {
	tilt_down 	= 0x04,
	tilt_up 	= 0x05,
	tilt_stop 	= 0x06,
	tilt_to     = 0x07,
	tilt_status = 0xAA
}Elevation_Motor_Command;

typedef enum {
	tilting_down 	= 0xD0,
	tilting_up 		= 0xE0,
	tilting_stopped = 0xF0
}Elevation_Motor_Status;

typedef enum {
	rotate_right 	= 0x08,
	rotate_left 	= 0x09,
	rotation_stop 	= 0x10,
	rotate_by   	= 0x20,
	calibrate	 	= 0x30,
	rotation_status = 0xAA
}Azimuth_Motor_Command;

typedef enum {
	rotating_right 		= 0x60,
	rotating_left 		= 0x70,
	rotation_stopped 	= 0x80,
	calibration_complete = 0x90
}Azimuth_Motor_Status;

typedef enum {
	limit_closed = 0x01,
	limit_opened = 0x02,
	limit_middle = 0x03
}Limit_Switch_Status;

uint8_t tilt_angle;
int16_t rotate_angle;
Limit_Switch_Status exp_limit_state;
Limit_Switch_Status elev_limit_state;
Expansion_Motor_Status exp_state;
Elevation_Motor_Status elev_state;
Azimuth_Motor_Status azi_state;
bool calibration_done;
bool calibration_on;

Limit_Switch_Status Get_Expansion_Status(void);
void Expansion_Open(void);
void Expansion_Close(void);
void Expnasion_Stop(void);

Limit_Switch_Status Get_Elevation_Status(void);
void Tilt_Down(void);
void Tilt_Up(void);
void Tilt_To(uint8_t angle);
void Tilt_Stop(void);

void Get_Azimuth_Status(void);
void Rotate_Right(void);
void Rotate_Left(void);
void Rotate_By(uint16_t angle);
void Rotation_Stop(void);
void Calibrate_Azimuth_Motor(void);

#endif /* MOTOR_H_ */
