/*
 * spi.h
 *
 *  Created on: May 8, 2018
 *      Author: Kunal Bhadane
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdbool.h>
#include <stdint.h>
#include "stm32f4xx_hal.h"

#include "motor.h"

typedef enum {
	AZIMUTH_NSS_PIN,
	ELEVATION_NSS_PIN,
	EXPANSION_NSS_PIN
}NSS_PIN;

#define TX_RX_BUFFER_SIZE 2
uint8_t aTxBuffer[TX_RX_BUFFER_SIZE];
uint8_t aRxBuffer[TX_RX_BUFFER_SIZE];

void SPI_Init(void);
void Expansion_SPI_Process(Expansion_Motor_Command mcommand);
void Elevation_SPI_Process(Elevation_Motor_Command mcommand);
void Azimuth_SPI_Process(Azimuth_Motor_Command mcommand);
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi);
void Button_Callback(void);

#endif /* SPI_H_ */
