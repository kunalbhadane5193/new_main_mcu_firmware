/*
 * timer.h
 *
 *  Created on: May 10, 2018
 *      Author: User
 */

#ifndef TIMER_H_
#define TIMER_H_

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>

#include "stm32f4xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern TIM_HandleTypeDef htim10;
extern TIM_HandleTypeDef htim11;

volatile bool time_updated;
volatile bool one_sec_updated;
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

extern void _Error_Handler(char *, int);

void TIM10_Init(void);
void TIM11_Init(void);
void Timer10_Start(void);
void Timer10_Stop(void);
void Timer11_Start(void);
void Timer11_Stop(void);

#endif /* TIMER_H_ */
