/*
 * automode.h
 *
 *  Created on: May 31, 2018
 *      Author: User
 */

#ifndef AUTOMODE_H_
#define AUTOMODE_H_

#include <stdbool.h>
#include "stm32f4xx_hal.h"

typedef enum {
	calibrate_azimuth_motor,
	wait_for_calibration,
	find_north,
	start_suntracking,
	wait_for_timeup,
	stop_auto_mode
}automode_commands;

automode_commands auto_command;
bool automode_on;
uint8_t auto_open_time[3];
uint8_t auto_close_time[3];
bool opened_by_autoopen;
bool closed_by_autoclose;

void Automode_Button_Callback(void);
void Stop_Auto_Mode(void);
void Set_Auto_Open_Time(uint8_t hour, uint8_t minutes);
void Set_Auto_Close_Time(uint8_t hour, uint8_t minutes);
void Set_Suntracking_Parameters(void);
void Start_Tracking(void);

#endif /* AUTOMODE_H_ */
