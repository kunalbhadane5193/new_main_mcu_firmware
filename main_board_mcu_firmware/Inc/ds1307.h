/*
 * ds1307.h
 *
 *  Created on: May 9, 2018
 *      Author: Kunal Bhadane
 */

#ifndef DS1307_H_
#define DS1307_H_

#include <stdbool.h>
#include <string.h>

#include "stm32f4xx_hal.h"

#define DS1307_ADDRESS 0x68 << 1

typedef enum {
	DS1307_REGISTER_SECONDS,
	DS1307_REGISTER_MINUTES,
	DS1307_REGISTER_HOURS,
	DS1307_REGISTER_DAY,
	DS1307_REGISTER_DATE,
	DS1307_REGISTER_MONTH,
	DS1307_REGISTER_YEAR,
	DS1307_REGISTER_CONTROL
}DS1307_Register;

typedef enum {
	TIME_SECOND,
	TIME_MINUTE,
	TIME_HOUR,
	TIME_DAY,
	TIME_DATE,
	TIME_MONTH,
	TIME_YEAR
}Time;

#define TIME_ARRAY_LENGTH 7
uint8_t current_time[TIME_ARRAY_LENGTH];
uint8_t readTime[TIME_ARRAY_LENGTH];

void I2C_Init(void);
void Read_Time(void);

#endif /* DS1307_H_ */
